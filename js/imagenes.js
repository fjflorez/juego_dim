var img_src = {
	// Inicio
	"play" : "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351645/imagenes_juego/play.png",
	// Tutorial
	"ayuda": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351627/imagenes_juego/ayuda.png",
	"ayuda0": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506519691/imagenes_juego/ayuda0.jpg",
	"ayuda1": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506519691/imagenes_juego/ayuda1.jpg",
	"ayuda2": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506519691/imagenes_juego/ayuda2.png",
	"ayuda3": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506519691/imagenes_juego/ayuda3.png",
	"ayuda4": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506519691/imagenes_juego/ayuda4.png",

	// Gameplay
	"background": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506482776/imagenes_juego/background.png",
	"sprite_pj": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351629/imagenes_juego/sprite_pj.png",
	"sprite_boss": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351629/imagenes_juego/sprite_boss.png",
	"estaca": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506520630/imagenes_juego/estaca.png",
	"prision": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506520719/imagenes_juego/prision.png",

	// Menu de pausa
	"pausa": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351629/imagenes_juego/pausa.png",
	"pausado": "http://res.cloudinary.com/deavvi9sj/image/upload/v1506351628/imagenes_juego/pausado.png",
};