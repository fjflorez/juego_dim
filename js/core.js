(function() {
    var anim,anim2,anim3;
    window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||  window.webkitRequestAnimationFrame || window.oRequestAnimationFrame;
    String.prototype.escape = function() {
        var tagsToReplace = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;'
        };
        return this.replace(/[&<>]/g, function(tag) {
            return tagsToReplace[tag] || tag;
        });
    };

    var IZQUIERDA = 37,
        ARRIBA = 38,
        DERECHA = 39,
        ABAJO = 40;

    window.addEventListener("resize", resizeCanvas, false);
    window.addEventListener("keydown", detenerEventos, false);

    function resizeCanvas() {
        var canvas_width = window.innerWidth;
        var canvas_height = window.innerHeight;

        drawCanvas( canvas_width, canvas_height );
    }

    function loadImages(sources, callback) {
        var loaded_imgs = 0;
        var num_imgs = 0;
        for(var i in sources) {
            num_imgs++;
        }

        for(var src in sources) {
            images[src] = new Image();
            images[src].onload = function() {
                if(++loaded_imgs >= num_imgs) {
                    callback(images);
                }
            };

            images[src].src = sources[src];
        }
    }

    function drawCanvas(canvas_width, canvas_height) {
        stage = new Kinetic.Stage({
            container: 'container',
            width: canvas_width,
            height: canvas_height
        });

        // Capas
        game_layer = new Kinetic.Layer();
        background_layer = new Kinetic.Layer();
        pj_layer = new Kinetic.Layer();
        boss_layer = new Kinetic.Layer();
        ui_layer = new Kinetic.Layer();
        pausa_layer = new Kinetic.Layer();
        prision_layer = new Kinetic.Layer();
        obstaculos1_layer = new Kinetic.Layer();
        obstaculos2_layer = new Kinetic.Layer();
        obstaculos3_layer = new Kinetic.Layer();

        // Capas - Escaleras
        escaleras_layers = new Array(3);
        for(var i = 0; i < 3; i++) {
            escaleras_layers[i] = new Kinetic.Layer();
        }

        // Capas - Letreros
        letreros_layer = new Array(6);
        for(var i = 0; i < letreros_layer.length; i++) {
            letreros_layer[i] = new Kinetic.Layer();
        }

        // Imagenes
        var background_img = new Kinetic.Image({
            x: 0,
            y: 0,
            image: images.background,
            width: 1300,
            height: 600
        });

        var prision_img = new Kinetic.Image({
            x:70,
            y: 0,
            image: images.prision,
            width: 200,
            height: 150
        });

        var pausa_img = new Kinetic.Image({
            x: 1000,
            y:0,
            image: images.pausa
        });

        var obstaculos1_img = new Kinetic.Image({
            x: 400,
            y: 0,
            image: images.obstaculo
        });

        var obstaculos3_img = new Kinetic.Image({
            x: 800,
            y: 0,
            image: images.obstaculo
        });
        
        var obstaculos2_img = new Kinetic.Image({
            x:600,
            y:0,
            image: images.obstaculo
        });

        
        pausa_layer.on("click", function(e){ 
            pausado_layer = new Kinetic.Layer();
            var pausado_img = new Kinetic.Image({
                x: 150,
                y: 100,
                image: images.pausado,
                width:1000,
                height:500
            })
            background_layer.add(background_img);
            pausado_layer.add(pausado_img);
           
            stage.add(background_layer)
            stage.add(pausado_layer);
            anim = new Kinetic.Animation(function(frame) {
                obstaculos1_img.attrs.y= 0
                obstaculos1_img.setY(450*Math.sin(frame.time * 1* Math.PI / 000) );
            }, obstaculos1_layer);

            anim.start();
            anim4 = new Kinetic.Animation(function(frame) {
                obstaculos2_img.attrs.y = 0;
                obstaculos2_img.setY(450*Math.sin(frame.time * 1* Math.PI / 0) );
            }, obstaculos2_layer);

            anim4.start();
            anim5 = new Kinetic.Animation(function(frame) {
                obstaculos3_img.attrs.y= 0;
                obstaculos3_img.setY(450*Math.sin(frame.time * 1* Math.PI / 000) );
            }, obstaculos3_layer);

            anim5.start();
            pausado_layer.on("click", function(e) {
                adicionObjetos()
                anim1.start()
                anim2.start()
                anim3.start()
            });
        });

        anim1 = new Kinetic.Animation(function(frame) {
            obstaculos1_img.attrs.y= 0
            obstaculos1_img.setY(450*Math.sin(frame.time * 1* Math.PI / 2000) );
        }, obstaculos1_layer);

        anim1.start();

        anim2= new Kinetic.Animation(function(frame) {
            obstaculos2_img.attrs.y= 0
            obstaculos2_img.setY(450*Math.sin(frame.time * 1* Math.PI / 1600) );
        }, obstaculos2_layer);

        anim2.start();

        anim3 = new Kinetic.Animation(function(frame) {
            obstaculos3_img.attrs.y= 0
            obstaculos3_img.setY(450*Math.sin(frame.time * 1* Math.PI / 1800) )
        }, obstaculos3_layer);

        anim3.start();

        var text_puntos = new Kinetic.Text({
            x: 120,
            y: 1,
            text: 'PUNTOS: 0',
            fontSize: 32,
            fontFamily: 'Calibri',
            fill: '#FFF',
            width: 380,
            align: 'center'
        });

        ui_layer.add( text_puntos );

        // Figuras geometricas invisibles
        // Escaleras
        escaleras = [
            new Kinetic.Rect({
                x: 1225, 
                y: 23,
                height: 200,
                width: 50
            }),
            new Kinetic.Rect({
                x: 25,
                y: 173,
                height: 200,
                width: 50
            }),
            new Kinetic.Rect({
                x: 1220,
                y: 333,
                height: 200,
                width: 50
            })
        ];

        // letreros
        letreros =  [
            new Kinetic.Rect({
                x: 215,
                y: 220,
                height: 50,
                width: 50
            }),
            new Kinetic.Rect({
                x: 1050,
                y: 220,
                height: 50,
                width: 50
            }),
            new Kinetic.Rect({
                x: 230,
                y: 380,
                height: 50,
                width: 50
            }),
            new Kinetic.Rect({
                x: 980,
                y: 380,
                height: 50,
                width: 50
            }),
            new Kinetic.Rect({
                x: 240,
                y: 530,
                height: 50,
                width: 50
            }),
            new Kinetic.Rect({
                x: 1080,
                y: 530,
                height: 50,
                width: 50
            })
        ];

        // Clase para el personaje
        function Character(_x, _y) {
            this.sizes = [64, 128];
            this.sprite_img = new Kinetic.Sprite({
                x: _x,
                y: _y,
                image: images.sprite_pj,
                animation: 'reset_right',
                animations: {
                    reset_right: [
                        // x, y, width, height
                        9, 0, this.sizes[0], this.sizes[1]
                    ],
                    reset_left: [
                        474, 259, this.sizes[0], this.sizes[1]
                    ],
                    right: [
                        103, 0, this.sizes[0], this.sizes[1],
                        222, 0, this.sizes[0], this.sizes[1],
                        328, 0, this.sizes[0], this.sizes[1],
                        222, 0, this.sizes[0], this.sizes[1]
                    ],
                    left: [
                        346, 260, this.sizes[0], this.sizes[1],
                        242, 260, this.sizes[0], this.sizes[1],
                        130, 260, this.sizes[0], this.sizes[1],
                        6, 260, this.sizes[0], this.sizes[1]
                    ],
                    up: [
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1],
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1]
                    ],
                    down: [
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1],
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1]
                    ]
                },
                frameRate: 10,
                frameIndex: 0
            });

            game_layer.add(this.sprite_img);
            this.sprite_img.start();

            this.getX = function() {
                return this.sprite_img.getPosition().x;
            };

            this.getY = function() {
                return this.sprite_img.getPosition().y;
            };

            this.setX = function(_x) {
                this.sprite_img.x(_x);
            };

            this.setY = function(_y) {
                this.sprite_img.y(_y);
            };

            this.setPos = function(_x, _y) {
                this.sprite_img.x(_x);
                this.sprite_img.y(_y);
            };

            this.setThrust = function(_thrust) {
                switch(_thrust) {
                    case "reset":
                        if(pj_invertido) {
                            this.sprite_img.animation('reset_left');

                        } else {
                            this.sprite_img.animation('reset_right');
                        }
                    break;

                    case "left":
                        this.sprite_img.animation('left');
                    break;

                    case "up":
                        this.sprite_img.animation('up');
                    break;

                    case "right":
                        this.sprite_img.animation('right');
                    break;

                    case "down":
                        this.sprite_img.animation('down');
                    break;
                }
            };

            this.getPosition = function() {
                return this.sprite_img.getPosition();
            };

            this.getWidth = function() {
                return this.sizes[0];
            };

            this.getHeight = function() {
                return this.sizes[1];
            };
        }

        var pj = new Character(100, 480);
        
        // Clase para el boss
        function Boss(_x, _y) {
            this.sizes = [64, 128];
            this.sprite_img = new Kinetic.Sprite({
                x: _x,
                y: _y,
                image: images.sprite_boss,
                animation: 'reset_right',
                animations: {
                    reset_right: [
                        // x, y, width, height
                        9, 0, this.sizes[0], this.sizes[1]
                    ],
                    reset_left: [
                        474, 259, this.sizes[0], this.sizes[1]
                    ],
                    right: [
                        103, 0, this.sizes[0], this.sizes[1],
                        222, 0, this.sizes[0], this.sizes[1],
                        328, 0, this.sizes[0], this.sizes[1],
                        222, 0, this.sizes[0], this.sizes[1]
                    ],
                    left: [
                        346, 260, this.sizes[0], this.sizes[1],
                        242, 260, this.sizes[0], this.sizes[1],
                        130, 260, this.sizes[0], this.sizes[1],
                        6, 260, this.sizes[0], this.sizes[1]
                    ],
                    up: [
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1],
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1]
                    ],
                    down: [
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1],
                        476, 125, this.sizes[0], this.sizes[1],
                        0, 395, this.sizes[0], this.sizes[1]
                    ]
                },
                frameRate: 10,
                frameIndex: 0
            });

            game_layer.add(this.sprite_img);
            this.sprite_img.start();

            this.getX = function() {
                return this.sprite_img.getPosition().x;
            };

            this.getY = function() {
                return this.sprite_img.getPosition().y;
            };

            this.setX = function(_x) {
                this.sprite_img.x(_x);
            };

            this.setY = function(_y) {
                this.sprite_img.y(_y);
            };

            this.setPos = function(_x, _y) {
                this.sprite_img.x(_x);
                this.sprite_img.y(_y);
            };

            this.setThrust = function(_thrust) {
                switch(_thrust) {
                    case "reset":
                        if(pj_invertido) {
                            this.sprite_img.animation('reset_left');

                        } else {
                            this.sprite_img.animation('reset_right');
                        }
                    break;

                    case "left":
                        this.sprite_img.animation('left');
                    break;

                    case "up":
                        this.sprite_img.animation('up');
                    break;

                    case "right":
                        this.sprite_img.animation('right');
                    break;

                    case "down":
                        this.sprite_img.animation('down');
                    break;
                }
            };

            this.getPosition = function() {
                return this.sprite_img.getPosition();
            };

            this.getWidth = function() {
                return this.sizes[0];
            };

            this.getHeight = function() {
                return this.sizes[1];
            };
        }

        var boss = new Boss(150, 20);

        // Animación para mover
        function moverPj(dir) {
            if(!move_pj) {
                pj.setThrust(dir);
                move_pj = true;
                adicionObjetos();
                if( !anim1.start() || 
                    !anim2.start() ||
                    !anim3.start()) {
                    anim1.start();
                    anim2.start();
                    anim3.start();
                }
            }
        }

        // Detectar interseccion de escalera y personaje
        function toggleEscaleras() {
            for(var i = 0, j = escaleras_layers.length; i < j; i++) {
                var pos = pj.getPosition();
                
                if( pos.x == escaleras[i].attrs.x && pos.y < escaleras[i].attrs.y ) {
                    pos.y = escaleras[i].attrs.y;
                    pj.setPos(pos.x, pos.y);
                }
                
                subir_escaleras = escaleras_layers[i].getIntersection( pos );

                if(subir_escaleras) {
                    break;
                }
            }
        }
        
        // Colision obstaculos
        setInterval(function() {
            var pjx = pj.getPosition().x;
            var pjy = pj.getPosition().y;
            var obsx= obstaculos1_img.getPosition().x;
            var obsy1= obstaculos1_img.getPosition().y;
            var obsy2= obstaculos2_img.getPosition().y;
            var obsy3= obstaculos3_img.getPosition().y;

            if (obsy1 >=356&& obsy1 <=400 && pjx>=370 && pjx<=490 && pjy== 480|| 
            obsy2 >=356&& obsy2 <=400 && pjx>=570 && pjx<=690 && pjy== 480|| 
            obsy3 >=356&& obsy3 <=400 && pjx>=770 && pjx<=890 && pjy== 480||
            obsy1 >=200 && obsy1<=420 && pjx>=370 && pjx<=490 && pjy>= 300 && pjy<= 330|| 
            obsy2 >=200 && obsy2<=420 && pjx>=570 && pjx<=690 && pjy>= 300 && pjy<= 330|| 
            obsy3 >=200 && obsy3<=420 && pjx>=770 && pjx<=890 && pjy>= 300 && pjy<= 330||
            obsy1 >=0 && obsy1<= 270 && pjx>=370 && pjx<=490 && pjy>= 180 && pjy<= 150|| 
            obsy2 >=0 && obsy2<=270 && pjx>=570 && pjx<=690  && pjy>= 180 && pjy<= 150||
            obsy3 >=0 && obsy3<=270 && pjx>=770 && pjx<=890  && pjy>= 180 && pjy<= 150
            ) {
                alert("GAME OVER")
                var href = "juego.html";
                window.location.href = href;
            } 
        }, 1000 / 1000);

        // Detener movimiento en bordes
        function hitBorder(step, pj) {
            // Evitar cruce derecho
            var suma = step + pj.getWidth();
            if( suma > background_img.attrs.width ) {
                return true;
            
            // Evitar cruce izquierdo
            } else if( suma - pj.getWidth() < 0 ) {
                return true;
            }

            return false;
        }

        function getNumRandom(min, max) {
            max++;
            return Math.floor((Math.random() * (max - min)) + min);
        }

        function getPregunta() {
            var length_preguntas = preguntas.length;
            var preg = preguntas.splice( getNumRandom(0, length_preguntas), 1 );

            return preg[0];
        }

        // UI
        /**
         * Funcion para realizar una acción al pasar por los letreros
        */
        function cruceLetreros() {
            var coordenadas_boc =  null;
            // Evitar cruce derecho
            for(var i = 0, j = letreros_layer.length; i < j; i++) {
                var pos = pj.getPosition();
                pos.y += letreros[i].attrs.height;

                coordenadas_boc = {
                    x: letreros[i].attrs.x - 140,
                    y: letreros[i].attrs.y - 225
                };
                
                show_letrero = letreros_layer[i].getIntersection( pos );

                if(show_letrero) {
                    if(!toggle_letrero) {
                        var preg = getPregunta();
                        var radio = $("<input />").attr({
                            "type": "radio"
                        });

                        var ul = ["<ul>", "</ul>"];
                        var li = ["<li ", "khe=", "</li>"];

                        var str_final = "";

                        str_final = ul[0];
                        for(var k = 0; k < preg.respuestas.length; k++) {
                            str_final += li[0]; 
                            str_final += li[1] + preg.respuestas[k].v_pregunta + ">";
                            str_final += preg.respuestas[k].respuesta.escape() + li[2];
                        }
                        str_final += ul[1];

                        mostrarBocadillo(preg.pregunta.escape(), str_final, coordenadas_boc);

                        letreros[i] = null;
                        letreros.splice(i, 1);

                        letreros_layer[i] = null;
                        letreros_layer.splice(i, 1);

                        console.log(i);
                        console.log(letreros);
                        console.log(letreros_layer);
                        break;
                    }
                }
            }
        }

        function updatePuntos(p) {
            text_puntos.setText("PUNTOS: " + p);
            ui_layer.draw();
        }

        function mostrarBocadillo(titulo, parrafo, coordenadas, callback) {
            toggle_letrero = true;

            bocadillo.css({
                "left": coordenadas.x,
                "top": coordenadas.y
            });

            bocadillo.find("#titulo").html(titulo);
            bocadillo.find("#pista").html(parrafo);

            bocadillo.show(700);
            bocadillo.find("#pista li").click(function() {
                if( $(this).attr("khe") == 1 ) {
                    puntos += 10;
                    updatePuntos(puntos);
                }

                bocadillo.hide("fast", function() {
                    bocadillo.find("#titulo").html("");
                    bocadillo.find("#pista").html("");

                    toggle_letrero = false;
                });
            });

            if(typeof callback === "function") {
                callback();
            }
        }

        // Eventos para caminar
        window.addEventListener("keydown", function(e) {
            var direccion = e.keyCode;
            var paso = 0;

            switch(direccion) {
                case IZQUIERDA:
                    moverPj('left');

                    if(!pj_invertido) {
                        pj_invertido = true;
                    }

                    paso = pj.getX() - pasos;
                    
                    if( !hitBorder(paso, pj) ) {
                        pj.setX( paso );
                        cruceLetreros();
                    }
                break;

                case ARRIBA:
                    toggleEscaleras();
                    paso = pj.getY() - pasos

                    if(subir_escaleras) {
                        moverPj('up');

                        pj.setY( paso );
                    }
                break;

                case DERECHA:
                    moverPj('right');

                    paso = pj.getX() + pasos;

                    if( !hitBorder(paso, pj) ) {
                        pj.setX( paso );
                        cruceLetreros();
                    }                    
                break;

                case ABAJO:
                    toggleEscaleras();

                    if(subir_escaleras) {
                        moverPj('down');

                        pj.setY(
                            pj.getY() + pasos
                        );
                    }
                break;
            }
        }, false);

        window.addEventListener("keyup", function(e) {
            pj.setThrust('reset');
            resetPj();
        }, false);

        function adicionObjetos() {
            // Adicion de objetos en capas
            background_layer.add(background_img);
            pausa_layer.add(pausa_img);


            // Adicion de capas
            stage.add(background_layer);
            stage.add(pausa_layer);

            prision_layer.add(prision_img);
            obstaculos1_layer.add(obstaculos1_img);
            obstaculos2_layer.add(obstaculos2_img);
            obstaculos3_layer.add(obstaculos3_img);
            // Adicion de capas
            stage.add(background_layer);
            stage.add(pausa_layer);
            stage.add(prision_layer);
            stage.add(game_layer);
            stage.add(obstaculos1_layer);
            stage.add(obstaculos2_layer);
            stage.add(obstaculos3_layer);
            stage.add(ui_layer);


            // adicion de objeto y capas para las escaleras
            for(var i = 0, j = escaleras_layers.length; i < j; i++) {
                escaleras_layers[i].add(escaleras[i]);
                stage.add( escaleras_layers[i] );
            }

            // adicion de objeto y capas para los letreros
            for(var i = 0, j = letreros_layer.length; i < j; i++) {
                letreros_layer[i].add(letreros[i]);
                stage.add( letreros_layer[i] );
            }
        }

        adicionObjetos();
    }

    function detenerEventos(e) {
        var key_code = e.keyCode;
        if( key_code == IZQUIERDA || 
            key_code == ARRIBA || 
            key_code == DERECHA ||
            key_code == ABAJO ) {
            e.preventDefault();
        }
    }

    function resetPj() {
        move_pj = false;
        pj_invertido = false;
    }

    var images = {};

    var img_sources = {
        background: img_src.background,
        sprite_pj: img_src.sprite_pj,
        sprite_boss: img_src.sprite_boss,
        pausa: img_src.pausa,
        pausado: img_src.pausado,       
        prision: img_src.prision,
        obstaculo: img_src.estaca
    };

    var stage;
    var game_layer, background_layer, pj_layer,
        boss_layer, ui_layer, escaleras_layer, letreros_layer;

    var aire = false;
    
    // Personaje
    var move_pj = false;
    var pj_invertido = false;
    var pasos = 10;//4;

    // Escaleras
    var escaleras = [];
    var subir_escaleras = false;

    // Letreros
    var letreros = [];
    var bocadillo = $("#bocadillo");
    var show_letrero = false;
    var toggle_letrero = false;

    // Preguntas
    var preguntas = {};
    var puntos = 0;

    loadImages(img_sources, function() {
        resizeCanvas();
    });


    $(document).ready(function() {
        // Obtener preguntas
        $.ajax({
            url: "php/Preguntas.php",
            method: "POST",
            data: {
                peticion: "get_preguntas",
                id_juego: 6
            },
            dataType: "JSON",
            success: function(res) {
                preguntas = res;

            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        });
    });

})();