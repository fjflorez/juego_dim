function Enemigo(x,y){
	Kinetic.Rect.call(this);
	this.setWidth(60);
	this.setHeight(60);
	this.setX(x);
	this.setY(y);
	this.setFill("blue");
	this.contador = 0;
	this.mover =  function(){
		this.contador++;
		this.setY(this.getY()+ Math.sin(this.contador* Math.PI / 50)*5);
	}

}
Enemigo.prototype = Object.create(Kinetic.Rect.prototype);