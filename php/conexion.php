<?php 

    class Conexion {
        private $datos_con;

        public function __construct() {
            $produccion = true;

            if($produccion) {
                $this->datos_con = Array(
                    "host" => "localhost",
                    "port" => "",
                    "dbname" => "id2966833_adsi",
                    "user" => "id2966833_adsi",
                    "pass" => "123456"
                );
            
            } else {
                $this->datos_con = Array(
                    "host" => "localhost",
                    "port" => .":".3306,
                    "dbname" => "juego_dim",
                    "user" => "root",
                    "pass" => ""
                );
            }
        }

        public function conectar() {
            return new PDO(
                "mysql:host=".$this->datos_con["host"]
                .$this->datos_con["port"]
                .";dbname=".$this->datos_con["dbname"], 
                $this->datos_con["user"], 
                $this->datos_con["pass"]
            );
        }
    }
?>