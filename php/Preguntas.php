<?php 
    include_once "conexion.php";

    class Preguntas {
        private $mensaje;
        private $nombre;
        private $password;
        private $email;
        private $puntaje;
        private $id_juego;

        public function set($var, $value) {
            $this->$var = $value;
        }

        public function get($var) {
            return $this->$var;
        }

        # StandBy
        public function insert(){
            $model = new Conexion();
            $conexion = $model -> conectar();
            $sql = "INSERT INTO usuarios(nombre, email, password, puntaje)";
            $sql .= "VALUES (:nombre, :email, :password, :puntaje)";
            $consulta = $conexion->prepare($sql);
            $consulta -> bindParam(':nombre', $this->nombre);
            $consulta -> bindParam(':password', $this->password);
            $consulta -> bindParam(':email', $this->email);
            $consulta -> bindParam(':puntaje', $this->puntaje);

            if(!$consulta){
                $this->mensaje = $conexion->errorInfo();
            }else{
                $consulta->execute();
                $this->mensaje = "Datos guardados con exito";
            }
        }

        public function getPregutas() {
            $model = new Conexion();
            $conexion = $model->conectar();

            $sSQL = "SELECT * FROM preguntas WHERE id_juego = :id_juego";

            $sentencia = $conexion->prepare( $sSQL );
            $sentencia->bindParam(":id_juego", $this->id_juego);

            $sentencia->execute();
            $arr_respuesta = Array();

            while($registro = $sentencia->fetch()) {
                array_push($arr_respuesta, Array(
                    "id_pregunta" => $registro["id_pregunta"],
                    "pregunta" => utf8_encode( $registro["pregunta"] ),
                    "respuestas" => self::getRespuestas( $registro["id_pregunta"] )
                ));
            }

            return $arr_respuesta;
        }

        private function getRespuestas($id_pregunta) {
            $model = new Conexion();
            $conexion = $model->conectar();

            $sSQL = "SELECT * FROM respuestas WHERE id_pregunta = :id_pregunta";

            $sentencia = $conexion->prepare( $sSQL );
            $sentencia->bindParam(":id_pregunta", $id_pregunta);

            $sentencia->execute();
            $arr_respuesta = Array();

            while($registro = $sentencia->fetch()) {
                array_push($arr_respuesta, Array(
                    "id_respuesta" => $registro["id_respuesta"],
                    "respuesta" => utf8_encode( $registro["respuesta"] ),
                    "v_pregunta" => $registro["v_pregunta"]
                ));
            }

            return $arr_respuesta;
        }
    }

    if( isset($_POST["peticion"]) ) {
        $peticion = $_POST["peticion"];
        $db_preguntas = new Preguntas();

        $respuesta = Array("msg" => "Error al procesar solicitud");

        switch($peticion) {
            case "get_preguntas":
                $id_juego = $_POST["id_juego"];
                $db_preguntas->set("id_juego", $id_juego);

                $respuesta = $db_preguntas->getPregutas();
            break;
        }

        echo json_encode($respuesta);
    }
?>